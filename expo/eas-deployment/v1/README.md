# EAS Deployment Workflow v1

## About this workflow

Updates, builds and submits apps via the eas cli automatically when matching following branch names

- [ ] main
- [ ] preview
- [ ] staging
- [ ] production

## Getting started

- Provide the `EXPO_TOKEN` variable to authenticate with the eas cli.
- Provide `EXPO_APPLE_ID`, `APP_STORE_CONNECT_KEY` & `GOOGLE_SERVICE_ACCOUNT_KEY` to submit builds (needs further testing).
- Include the [workflow](https://gitlab.com/3ts-public/3ts-gitlab-ci-workflows/-/raw/main/expo/eas-deployment/v1/.gitlab-ci.yml) in your own `.gitlab-ci.yml` by using

```yml
include:
  - remote: https://gitlab.com/3ts-public/3ts-gitlab-ci-workflows/-/raw/main/expo/eas-deployment/v1/.gitlab-ci.yml
```

Have a look at the provided [example workflow](https://gitlab.com/3ts-public/3ts-gitlab-ci-workflows/-/raw/main/expo/eas-deployment/v1/.example.yml).

## Branch configurations

There is no difference between EAS Branches and EAS Profiles in this workflow.
All configurations are 'optional'. If a branch does not exist the workflow is not applied.

### main (development)

- [ ] EAS Profile/Branch: **development**
- Creates two jobs (android & ios) to build development clients (`eas build`).
- Jobs need to be run manually (i.e. no pipeline starts automatically on push).

### preview

- [ ] EAS Profile/Branch: **preview** (_same as branch_)
- Automatically runs a job to update the preview branch on expo (`eas update`).
- Update message is equal to commit title. Update times out after 2 hours.
- Creates a manual job to create preview builds (`eas build`).

### staging & production

- [ ] EAS Profile/Branch: _same as branch_
- Automatically runs a job to update existing staging or production builds (`eas update`).
- **Notice:** Pushing on the production branch will **update production builds**. It is recommended to protect those branches.
- Update message is equal to commit title. Update times out after 2 hours.
- Creates three manual jobs to create and submit builds
  - `build`, which just runs `eas build`.
  - `submit`, which is available when the build job finishes and just runs `eas submit` on the latest build.
  - `build-and-submit`, which creates builds with the `--auto-submit` flag. Those builds are submitted automatically.
